// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
    // url - this is the address which the request is to be made and source where the response will come from (endpoint)
    // options - array or properties that contains the HTTP

// get post data/ retrieve/read function
fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json())
        .then(data => showPosts(data))

// view post - display each post from JSON 
const showPosts = posts =>{
    // contains all the posts
    let postEntries= ""
    posts.forEach(post => {
        // console.log(post)
        postEntries += `<div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>`
    });
    // To check what is stored in the postEntries variables.
    // console.log(postEntries)
    // To assign the value of postEntries to the element with "div-post-entries" id.
    document.querySelector('#div-post-entries').innerHTML=postEntries    
}
// Mini-Activity:
//     Retrieve a single post from JSON API and print it in the console.
fetch('https://jsonplaceholder.typicode.com/posts/1')
        .then(res => res.json())
        // .then(data => console.log(data))
fetch('https://jsonplaceholder.typicode.com/posts/2')
        .then(res => res.json())
        // .then(data => console.log(data))

// post data / create function
document.querySelector('#form-add-post').addEventListener("submit", e => {
    // Prevents the page from reloading. Also prevents the default behavior of our event.
    e.preventDefault()
    fetch('https://jsonplaceholder.typicode.com/posts/', {
        method: 'POST', 
        body: JSON.stringify({
            title: document.querySelector('#txt-title'),
            body: document.querySelector('#txt-body'),
            userId: 290
        }),
        headers: {"Content-Type":"application/json"}
    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            alert('Post Successfully Added!')
        })
    document.querySelector('#txt-title').value=null
    document.querySelector('#txt-body').value=null
})

const editPost= (id)=>{
    console.log(id)
    document.querySelector('#txt-edit-id').value=id
    document.querySelector('#txt-edit-title').value= document.querySelector(`#post-title-${id}`).innerHTML
    document.querySelector('#txt-edit-body').value= document.querySelector(`#post-body-${id}`).innerHTML
    document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

// Update post data / edit button
document.querySelector('#form-edit-post').addEventListener("submit", e => {
    // Prevents the page from reloading. Also prevents the default behavior of our event.
    e.preventDefault()
    let id = document.querySelector('#txt-edit-id').value
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'PUT', 
        body: JSON.stringify({
            id:id,
            // the value will be from the value of input fields or HTML elements
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 290
        }),
        headers: {"Content-Type":"application/json"}

    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            alert('Post Successfully Updated!')
        })
    // Reset input fields once submitted
    document.querySelector('#txt-edit-title').value=null
    document.querySelector('#txt-edit-body').value=null
    // Resetting disabled attribute for button
    document.querySelector('#btn-submit-update').setAttribute('disabled', true)
})



const deletePost= (id)=>{    
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: 'DELETE',
        headers: {"Content-Type": "application/json"}
    }).then(response => response.json())
    .then(json => console.log(json, `Successfully deleted post with id of ${id}`))
    document.querySelector(`#post-${id}`).remove()
}



