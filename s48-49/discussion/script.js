// mock data
let posts = []
// will be the id
let count = 1

// create post
document.querySelector("#form-add-post").addEventListener('submit', (e)=>{
    e.preventDefault()
    posts.push(
        {
            id: count,
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value
        }
    )
    count++
    // console.log(posts)
    showPosts(posts)
    // alert('success add')
})

// retrieve/read
const showPosts = (posts) =>{
    // contains all the posts
    let postEntries= ""
    console.log(posts)
    posts.forEach(post => {
        // console.log(post)
        postEntries += `<div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>`
    });
    // To check what is stored in the postEntries variables.
    // console.log(postEntries)
    // To assign the value of postEntries to the element with "div-post-entries" id.
    document.querySelector('#div-post-entries').innerHTML=postEntries    
}

const editPost= (id)=>{
    // console.log(id)
    document.querySelector('#txt-edit-id').value=id
    document.querySelector('#txt-edit-title').value= document.querySelector(`#post-title-${id}`).innerHTML
    document.querySelector('#txt-edit-body').value= document.querySelector(`#post-body-${id}`).innerHTML
    document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

// Update post data / edit button
document.querySelector('#form-edit-post').addEventListener("submit", e => {
    // Prevents the page from reloading. Also prevents the default behavior of our event.
    e.preventDefault()

    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first
        if(posts[i].id.toString()===document.querySelector('#txt-edit-id').value){
            // reassign
            posts[i].title = document.querySelector('#txt-edit-title').value
            posts[i].body = document.querySelector('#txt-edit-body').value

            showPosts(posts)
            alert('Post Successfully Updated!')
            // // Reset input fields once submitted
            document.querySelector('#txt-edit-title').value=null
            document.querySelector('#txt-edit-body').value=null
            // Resetting disabled attribute for button
            document.querySelector('#btn-submit-update').setAttribute('disabled', true)
            break
        }else{
            alert('error')
        }  
    }

    // let id=document.querySelector('#txt-edit-id').value
    // if(id < posts.length){
    //     document.querySelector(`#post-title-${id}`).innerHTML= document.querySelector('#txt-edit-title').value
    //     document.querySelector(`#post-body-${id}`).innerHTML= document.querySelector('#txt-edit-body').value
    // }

    // console.log(data)
    // alert('Post Successfully Updated!')
})