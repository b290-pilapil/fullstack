import { Fragment } from 'react'
import {Button, Col, Row} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Banner({errorPage}){
    // console.log(errorPage)
    return(
        <Row>
            <Col className='p-5'>
                {(errorPage==true)?
                    <Fragment>
                        <h1>Page not Found</h1>
                        <p>Go back to <Link to="/">homepage</Link>.</p>
                    </Fragment>
                :
                    <Fragment>
                        <h1>Zuitt Coding Bootcamp</h1>
                        <p>Opportunities for everyone, everywhere</p>
                        <Button variant='primary'>Enroll Now!</Button>
                    </Fragment>
                }
            </Col>
        </Row>
    )
}
// The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.