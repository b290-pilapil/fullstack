import { useState, useEffect } from 'react'
import {Card, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function CourseCard({course}){
    // Checks to see if the data was successfully passed
    // console.log(course)
    // Every component receives information in a form of an object
    // console.log(typeof course)
    const {_id, name, description, price} = course
    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
    //     const [getter, setter] = useState(initialGetterValue);
    const [count, setCount] = useState(0)
    const [seats, setSeats] = useState(5)
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
    // console.log(useState(0))
    // function enroll(){
        // if(seats<31){
        //     setCount(count+1)
        //     // console.log(`Enrollees: ${count}`)
        //     setSeats(seats-1)
        // }
        // }else{
        //     alert("No more available seats")
        // setCount(count+1)
        // setSeats(seats-1)
        
    // }
    // useEffect(()=>{
    //     if(seats===0){
    //         alert("No more available seats")
    //         // setSeats(false)
    //     }
    // }, [seats])

    return(
        <Card className="cardHighlight p-3 my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                {/* <Card.Text>Enrollees: {count}</Card.Text> */}
                {/* <Card.Text>Seats: {seats}</Card.Text> */}
                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}