import { Fragment, useContext } from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar(){
    // const [user,setUser]=useState(localStorage.getItem('email'))
    const {user} = useContext(UserContext)
    console.log(user)
    
    return (
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand as={NavLink} to='/'>Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls='basic-navbar-nav'/>
                <Navbar.Collapse id='basic-navbar-nav'>
                    <Nav className='ms-auto'>
                        {/* The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities. */}
                        {/* The "to" prop is used in place of the "href" prop for providing the URL for the page. */}
                        <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
                        <Nav.Link as={NavLink} to='/courses'>Courses</Nav.Link>
                        {(user.id)?
                            <Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
                        :
                            <Fragment>
                                <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
                                <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
                            </Fragment>
                        }                        
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
// React JS Components are independent, reusable pieces of code which normally contains JavaScript and JSX syntax which make up a part of our application.
// An example of this would be a navbar which contains several HTML elements that creates an interface for navigating through our application.
// The navbar is a component which makes up a part of our application hence it is called a component.
// React JS has two ways of implementing components which are namely "Class Components" and "Function components".
// In the bootcamp, we will be using "Function Components" meaning that we will be mainly focusing on functions in creating our React JS components compared to Classes.
// The reason for this is because Function Components are easier to digest for students.
// Both approaches are accepted when dealing with React JS.
// The naming convention for React JS components follows the "Pascal Case" having capitalized letters for all words of the function name AND the file name associated with it.
// The "export default" statements allow us to create a JavaScript module that will be used when the file is exported in a different component.

// React JS also applies the concepts of Rendering and Mounting in order to display and create components.
// "Rendering" refers to the process of calling/invoking a component returning a set of instructions for creating DOM.
// "Mounting" is when React JS "renders" or displays the component and builds the initial DOM based on the instructions.
// Unlike HTML tags where we use the lowercase letters for creating them, React JS components use the Pascal Case indicating that we are using a component instead of using an HTML tag.
// The "return" statement in a React JS component is what defines what will be rendered/displayed in our application.