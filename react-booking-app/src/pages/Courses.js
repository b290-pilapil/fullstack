import { Fragment, useEffect, useState } from 'react'

import CourseCard from '../components/CourseCard'
// import coursesData from '../data/coursesData'

export default function Courses(){
    // console.log(coursesData)
    // console.log(coursesData[0])

    // The "map" method loops through the individual course objects in our array and returns a component for each course
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
    // const courses = coursesData.map(course =>{
    //     return (
    //         <CourseCard key={course.id} course={course}/>
    //     )
    // })
    
    // State that will be used to store the courses retrieved from the database
    const [courses,setCourses] = useState([])

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/courses`)
            .then(res => res.json())
            .then(data=>{
                setCourses(data.map(course =>{
                    return (
                        <CourseCard key={course._id} course={course}/>
                    )
                }))
                // console.log(data)
            })
    },[])
    
    return (
        // <h1>Courses</h1>
        <Fragment>
            {courses}
        </Fragment>
    )
}