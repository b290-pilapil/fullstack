import { useContext, useEffect, useState } from 'react'
import {Form, Button} from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'

import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

    const {user, setUser} = useContext(UserContext)
    const navigate = useNavigate()

    // State hooks to store
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [moblieNumber, setMoblieNumber] = useState('')
    const [email, setEmail] = useState('')
    const [pass1, setPass1] = useState('')
    const [pass2, setPass2] = useState('')
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)
    
    useEffect(()=>{
        // Validation to enable submit button when all fields are populated and both passwords match
        if ((firstName!=='' && lastName!=='' && moblieNumber!=='' && email!=='' && pass1!=='' && pass2!=='') && (pass1===pass2)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[firstName, lastName, moblieNumber, email, pass1, pass2])

    // Function to simulate user registration
    function registerUser(e){
        e.preventDefault()
        fetch('http://localhost:4000/users/checkEmail', {
            // idk why POST to sa routes sa repo ni ms
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                email: email,
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if(!data){ //email not in DB
                    fetch('http://localhost:4000/users/register', {
                    method: 'POST',
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: moblieNumber,
                        email: email,
                        password: pass1
                        })
                    })
                        .then(res => res.json())
                        .then(regis_data => {
                            if(regis_data){ //success
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Registration Successful',
                                    text: 'Welcome to Zuitt!'
                                })
                                navigate('/login')
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Registration Failed',
                                    text: 'Please try again later.'
                                })
                            }
                        })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Duplicate Email Found',
                        text: 'Please provide a different email.',
                      })
                }
            })
        setFirstName('')
        setLastName('')
        setMoblieNumber('')
        setEmail('')
        setPass1('')
        setPass2('')
        // alert('Thank you for registering!')
    }

    return(
        (user.id)?
            <Navigate to='/courses'/>
        :
            <Form onSubmit={(e)=>registerUser(e)}>
                <Form.Group controlId='userFirstName'>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='Enter your first name'
                        value={firstName}
                        onChange={e =>setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId='userLastName'>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='Enter your last name'
                        value={lastName}
                        onChange={e =>setLastName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId='userMobileNum'>
                    <Form.Label>Mobile number</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='Enter mobile number'
                        value={moblieNumber}
                        onChange={e =>setMoblieNumber(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId='userEmail'>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type='email'
                        placeholder='Enter email'
                        value={email}
                        onChange={e =>setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className='text-muted'>We'll never share your email with anyone</Form.Text>
                </Form.Group>
                <Form.Group controlId='password1'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder='Enter a password'
                        value={pass1}
                        onChange={e =>setPass1(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId='password2'>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder='Confirm your password'
                        value={pass2}
                        onChange={e =>setPass2(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Conditional Rendering for submit button based on isActive state*/}
                {/* <Button variant='primary' type='submit' id='submitBtm' className='mt-3'>Register</Button> */}
                {isActive?
                    <Button variant='primary' type='submit' id='submitBtm' className='mt-3' >Register</Button>
                :
                    <Button variant='danger' type='submit' id='submitBtm' className='mt-3' disabled>Register
                    </Button>                
                }
                
            </Form>
    )
}