import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";

import UserContext from "../UserContext";

export default function Logout(){
    // localStorage.clear()

    const {unsetUser, setUser} = useContext(UserContext)
    // clear localStorage of the user's info
    unsetUser()

    useEffect(()=>{
        // Placing the "setUser" setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render a different component
        // By adding the useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user
        setUser({id: null})
    }, [])
    // redirect back to login
    return(
        <Navigate to={'/login'}/>
    )
}