const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

function updateSpan(event){
    spanFullName.innerHTML = txtFirstName.value + ' '+ txtLastName.value
}
txtLastName.addEventListener('keyup', updateSpan)
txtFirstName.addEventListener('keyup', updateSpan)