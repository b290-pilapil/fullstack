// querySelector() is a method that can be used to select a specific object/element from our document.
console.log(document.querySelector("#txt-first-name"))

// document refers to the whole page
console.log(document)

// Alternative ways to access HTML elements. This is what we can use aside from the querySelector().
// 	document.getElementById("txt-first-name");
// 	document.getElementsByClassName("txt-first-name");
// 	document.getElementsByTagName("input");

console.log(document.getElementById("txt-first-name"))

// Events & Event Listeners
const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

console.log(txtFirstName.value)
console.log(txtLastName.value)
console.log(spanFullName)

    // Event
    //     ex: click, hover, keypress and many other events
    // Event Listeners
    //     Allows us to let our user/s interact with our page. With each click or hover there is an event which triggers a function/task.
    // Syntax:
    //     selectedElement.addEventListener("event", function);


txtFirstName.addEventListener('keyup', (event)=>{
    // "innerHTML" property retrieves the HTML content/children within the element
    // "value" property retrieves the value from the HTML element
    spanFullName.innerHTML = txtFirstName.value
})

function printLastName(event){
    console.log(txtLastName.value)
    spanFullName.innerHTML = txtLastName.value
}
txtLastName.addEventListener('keyup', printLastName)

txtFirstName.addEventListener('keyup', (event)=>{
    // The "event" argument contains the information on the triggered event.
    console.log(event)
    // The "event.target" contains the element where the event happened.
    console.log(event.target)
    // the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
    console.log(event.target.value)
})

// mini activity
    // create enevent listener
txtFirstName.addEventListener('click', ()=>{
    alert('You click First name label')
})